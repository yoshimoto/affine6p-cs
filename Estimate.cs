﻿using System;
using System.Collections.Generic;

namespace Affine6P
{
    public class Affine6P
    {
        public static double[] Estimate(
            List<Tuple<double, double>> origin, List<Tuple<double, double>> convrt)
        {
            if (origin.Count != convrt.Count)
            {
                throw new ArgumentException("Number of items is not same.");
            }
            if (origin.Count >= 3)
            {
                return EstimateFull(origin, convrt);
            }
            else if (origin.Count == 2)
            {
                return EstimateHelmert(origin, convrt);
            }
            else if (origin.Count == 1)
            {
                return new double[6] { 1.0, 0.0, 0.0, 1.0, convrt[0].Item1 - origin[0].Item1, convrt[0].Item2 - origin[0].Item2 };
            }
            else
            {
                throw new ArgumentException("Number of items is less than one.");
            }
        }
        static double[] EstimateFull(List<Tuple<double, double>> origin, List<Tuple<double, double>> convrt)
        {
            int N = Math.Min(origin.Count, convrt.Count);

            double[][] mat = new double[3][];
            for (int i = 0; i < 3; i++)
            {
                mat[i] = new double[3] { 0, 0, 0 };
            }
            double[] vec = new double[6] { 0, 0, 0, 0, 0, 0 };
            for (int i = 0; i < N; i++)
            {
                double ox = (origin[i]).Item1;
                double oy = (origin[i]).Item2;
                double cx = (convrt[i]).Item1;
                double cy = (convrt[i]).Item2;

                mat[0][0] += ox * ox;
                mat[0][1] += ox * oy;
                mat[0][2] += ox;
                mat[1][0] += ox * oy;
                mat[1][1] += oy * oy;
                mat[1][2] += oy;
                mat[2][0] += ox;
                mat[2][1] += oy;
                mat[2][2] += 1;

                vec[0] += ox * cx;
                vec[1] += oy * cx;
                vec[2] += cx;
                vec[3] += ox * cy;
                vec[4] += oy * cy;
                vec[5] += cy;
            }
            double det = mat[0][0] * mat[1][1] * mat[2][2] +
                mat[1][0] * mat[2][1] * mat[0][2] +
                mat[2][0] * mat[0][1] * mat[1][2] -
                mat[0][0] * mat[2][1] * mat[1][2] -
                mat[2][0] * mat[1][1] * mat[0][2] -
                mat[1][0] * mat[0][1] * mat[2][2];
            if (Math.Abs(det) < 1e-8)
            {
                throw new DivideByZeroException();
            }
            double inv_det = 1.0 / det;
            double[][] inv_mat = new double[3][];
            for (int i = 0; i < 3; i++)
            {
                inv_mat[i] = new double[3] { 0, 0, 0 };
            }
            inv_mat[0][0] = inv_det * (mat[1][1] * mat[2][2] - mat[1][2] * mat[2][1]);
            inv_mat[0][1] = inv_det * (mat[1][2] * mat[2][0] - mat[1][0] * mat[2][2]);
            inv_mat[0][2] = inv_det * (mat[1][0] * mat[2][1] - mat[1][1] * mat[2][0]);
            inv_mat[1][1] = inv_det * (mat[2][2] * mat[0][0] - mat[2][0] * mat[0][2]);
            inv_mat[1][2] = inv_det * (mat[2][0] * mat[0][1] - mat[2][1] * mat[0][0]);
            inv_mat[2][2] = inv_det * (mat[0][0] * mat[1][1] - mat[0][1] * mat[1][0]);
            inv_mat[1][0] = inv_mat[0][1];
            inv_mat[2][0] = inv_mat[0][2];
            inv_mat[2][1] = inv_mat[1][2];
            var affine = new double[6];
            affine[0] = inv_mat[0][0] * vec[0] + inv_mat[0][1] * vec[1] + inv_mat[0][2] * vec[2];
            affine[1] = inv_mat[1][0] * vec[0] + inv_mat[1][1] * vec[1] + inv_mat[1][2] * vec[2];
            affine[2] = inv_mat[0][0] * vec[3] + inv_mat[0][1] * vec[4] + inv_mat[0][2] * vec[5];
            affine[3] = inv_mat[1][0] * vec[3] + inv_mat[1][1] * vec[4] + inv_mat[1][2] * vec[5];
            affine[4] = inv_mat[2][0] * vec[0] + inv_mat[2][1] * vec[1] + inv_mat[2][2] * vec[2];
            affine[5] = inv_mat[2][0] * vec[3] + inv_mat[2][1] * vec[4] + inv_mat[2][2] * vec[5];
            return affine;
        }
        static double[] EstimateHelmert(List<Tuple<double, double>> origin, List<Tuple<double, double>> convrt)
        {
            const int N = 2;
            double x0 = default;
            double y0 = default;
            double x1 = default;
            double y1 = default;
            double x02 = default;
            double y02 = default;
            double x0x1 = default;
            double y0y1 = default;
            double y0x1 = default;
            double x0y1 = default;
            for (int i = 0; i < N; i++)
            {
                double ox = origin[i].Item1;
                double oy = origin[i].Item2;
                double cx = convrt[i].Item1;
                double cy = convrt[i].Item2;
                x0 += ox;
                y0 += oy;
                x1 += cx;
                y1 += cy;
                x02 += ox * ox;
                y02 += oy * oy;
                x0x1 += ox * cx;
                x0y1 += ox * cy;
                y0x1 += oy * cx;
                y0y1 += oy * cy;
            }
            double det = x0 * x0 + y0 * y0 - N * (x02 + y02);
            if (Math.Abs(det) < 1e-8)
            {
                throw new DivideByZeroException();
            }
            var inv_det = 1.0 / det;
            var affine = new double[6] { 1.0, 0.0, 0.0, 1.0, 0.0, 0.0 };
            affine[0] = (x0 * x1 + y0 * y1 - N * (x0x1 + y0y1)) * inv_det;
            affine[1] = (y0 * x1 - x0 * y1 - N * (y0x1 - x0y1)) * inv_det;
            affine[2] = -affine[1];
            affine[3] = affine[0];
            affine[4] = (x1 - affine[0] * x0 - affine[1] * y0) / N;
            affine[5] = (y1 - affine[0] * y0 + affine[1] * x0) / N;
            return affine;
        }
        public static double EstimteError(double[] affine, List<Tuple<double, double>> origin, List<Tuple<double, double>> convrt)
        {
            int N = Math.Min(origin.Count, convrt.Count);
            double se = 0;
            for (int i = 0; i < N; i++)
            {
                var trans = Transform(affine, origin[i]);
                double dx = trans.Item1 - convrt[i].Item1;
                double dy = trans.Item2 - convrt[i].Item2;
                se += dx * dx + dy * dy;
            }
            return Math.Sqrt(se / N);
        }
        public static Tuple<double, double> Transform(double[] affine, Tuple<double, double> point)
        {
            double X = affine[0] * point.Item1 + affine[1] * point.Item2 + affine[4];
            double Y = affine[2] * point.Item1 + affine[3] * point.Item2 + affine[5];
            return new Tuple<double, double>(X, Y);
        }
        public static Tuple<double, double> TransformInv(double[] affine, Tuple<double, double> point)
        {
            double det = affine[0] * affine[3] - affine[2] * affine[1];
            double X = (affine[3] * (point.Item1 - affine[4]) - affine[1] * (point.Item2 - affine[5])) / det;
            double Y = -1.0 * (affine[2] * (point.Item1 - affine[4]) - affine[0] * (point.Item2 - affine[5])) / det;
            return new Tuple<double, double>(X, Y);
        }
    }
}
